# Virtual Rubik's Cube

Interactive, graphical Rubik's Cube. Written in Turbo Basic for my
undergraduate computer graphics course at Marquette University circa 1989.
